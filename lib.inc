section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte[rdi+rax], 0
    je .end

    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n'


; Принимает код символа и выводит его в stdout
print_char:
    push rdi

    mov rax, 1
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    syscall

    pop rdi

    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r13
    mov r13, rsp
    push 0
    mov rax, rdi            ; store in rax current number
    mov rsi, 10
.loop:
    xor rdx, rdx
    div rsi
    add rdx, '0'            ; rdx stores remainder
    dec rsp
    mov byte[rsp], dl
    and rax, rax            ; check if rax > 0
    jnz .loop

    mov rdi, rsp
    push r13
    call print_string
    pop rsp
    pop r13

    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    
    mov r9, rdi
    mov rdi, '-'
    push r9
    call print_char
    pop rdi
    neg rdi
    jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx
.loop:
    mov al, byte[rdi+rcx]
    cmp byte[rsi+rcx], al
    jne .end

    inc rcx
    test al, al
    jnz .loop
    mov rax, 1
.end:
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rsi, rsp
    xor rax, rax        ; call read
    xor rdi, rdi
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax
    push rdi            ; save begining of buffer
    mov r9, rdi
.skip:
    push rsi
    push r9
    call read_char
    pop r9
    pop rsi
    
    cmp rax, 0
    je .error
    cmp rax, ' '
    je .skip
    cmp rax, 0x9
    je .skip
    cmp rax, '\n'
    je .skip

.read:
    dec rsi
    jz .error
    mov byte[r9], al
    inc r9

    push rsi
    push r9
    call read_char
    pop r9
    pop rsi

    cmp rax, 0
    je .end
    cmp rax, ' '
    je .end
    cmp rax, 0x9
    je .end
    cmp rax, '\n'
    je .end
    jmp .read

.end:
    mov byte[r9], 0
    pop rax
    mov rdx, r9
    sub rdx, rax        ; end - begin = length
    ret

.error:
    pop rdi
    xor rax, rax
    xor rdx, rdx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov rsi, 10
.loop:
    xor r8, r8
    mov r8b, byte[rdi+rcx]
    cmp r8b, 0x30
    jb .exit
    cmp r8b, 0x39
    ja .exit
    sub r8b, 0x30
    mul rsi
    add rax, r8
    inc rcx
    jmp .loop
.exit:
    mov rdx, rcx
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:                ; rdi - string, rsi - buffer, rdx - len(buffer)
    xor rax, rax
    cmp rax, rdx
    jae .error

.loop:
    cmp rdx, rax
    jle .end_zero

    mov dl, byte[rdi]
    mov byte[rsi], dl

    inc rsi
    inc rdi

    cmp dl, 0
    jne .loop

    ret

.error:
    xor rax, rax
    ret

.end_zero:
    xor rax, rax
    ret
